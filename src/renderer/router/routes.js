import ContainersPage from '@/views/containers/ContainersPage';
import ImagesPage from '@/views/images/ImagesPage';
import VolumesPage from '@/views/volumes/VolumesPage';

const meta = {
  default: {
    layout: 'SidebarLayout',
  },
};

export default [
  {
    name: 'containers',
    path: '/containers',
    component: ContainersPage,
    meta: {
      ...meta.default,
    },
  },
  {
    name: 'images',
    path: '/images',
    component: require('../views/containers/ContainersPage'),
    meta: {
      ...meta.default,
    },
  },
  {
    name: 'volumes',
    path: '/volumes',
    component: require('../views/containers/ContainersPage'),
    meta: {
      ...meta.default,
    },
  },
  {
    path: '*',
    redirect: '/containers',
  },
];
