import Vue from 'vue';
import {
  Aside,
  Button,
  Container,
  Header,
  Menu,
  MenuItem,
  Notification,
  Tooltip,
} from 'element-ui';
import fontawesome from '@fortawesome/fontawesome';
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome';
import solidIcons from '@fortawesome/fontawesome-free-solid';
import App from '@/App';
import router from '@/router';
import store from '@/store';

if (!process.env.IS_WEB) Vue.use(require('vue-electron'));
Vue.config.productionTip = false;

fontawesome.library.add(solidIcons);
Vue.component('font-awesome-icon', FontAwesomeIcon);
// Vue.component('spinner', Spinner);
// Vue.component('add-placeholder', AddPlaceholder);

Vue.use(Aside);
Vue.use(Button);
Vue.use(Container);
Vue.use(Header);
Vue.use(Menu);
Vue.use(MenuItem);
Vue.use(Tooltip);

Vue.prototype.$notify = Notification;

/* eslint-disable no-new */
new Vue({
  router,
  store,
  render: h => h(App),
}).$mount('#app');
